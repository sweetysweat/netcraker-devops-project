import os
from typing import List

from python_custom_modules.helpers import create_file_from_template, execute_command


class Terraform:
    def __init__(self, home: os):
        self.terraform_home = f"{home}/terraform"
        self.terraform_templates = f"{home}/python_templates/terraform_templates"
        self.ssh_user = None
        self.vms_ip = dict()

    def create_variable_tf(self):
        """Create variables.tf if it doesn't exist"""
        if not os.path.isfile(f"{self.terraform_home}/variables.tf"):
            """Create variables.tf if it doesn't exist"""
            print("Enter data for connection to your cloud provider (only yandex).\n")

            params = {
                "zone": input("Zone: "),
                "token": input("Token: "),
                "cloud_id": input("Cloud ID: "),
                "folder_id": input("Folder ID: "),
            }
            create_file_from_template(self.terraform_templates, "template_for_variables.txt",
                                      f"{self.terraform_home}/variables.tf", params)
            return "File with ssh user successfully created."
        return "File with variable.tf already exists."

    def create_ssh_user(self, public_ssh_key: str) -> str:
        """Create meta.txt (ssh user) if it doesn't exist"""
        if not os.path.isfile(f"{self.terraform_home}/meta.txt"):
            print("\nEnter data to create user for ssh.\n")
            params = {
                "username": input("Username: "),
                "key": public_ssh_key
            }
            self.ssh_user = params["username"]

            create_file_from_template(self.terraform_templates, "template_for_user.txt",
                                      f"{self.terraform_home}/meta.txt", params)
            return "File with ssh user successfully created."
        with open(f"{self.terraform_home}/meta.txt", 'r') as f:
            for line in f:
                if "name" in line:
                    self.ssh_user = line.strip().split()[-1]
                    return self.ssh_user

    def run_script(self):
        """Run terraform script to configure claud and get ip address from all VMs."""
        commands_list = ["init", "fmt", "validate", "plan", "apply -auto-approve"]

        for command in commands_list:
            result = execute_command(f"terraform -chdir={self.terraform_home} {command}")
            print(result)

            vms_ip = list()
            # We don't check what's the command cause apply is always the last one.
            for row in result.split("\n"):
                if "known after apply" not in row and "public_ip" in row:
                    vms_ip.append(row)
            self.get_ip_address(vms_ip)

    def get_ip_address(self, vms_ip: List[str]):
        for ip in vms_ip:
            ip = ip.strip().split(" = ")
            if "jenkins" in ip[0]:
                self.vms_ip["jenkins"] = ip[-1].replace('"', '')
            if "liferay" in ip[0]:
                self.vms_ip["liferay"] = ip[-1].replace('"', '')
            if "postgres" in ip[0]:
                self.vms_ip["postgres"] = ip[-1].replace('"', '')
