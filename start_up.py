import os
import socket
from time import sleep
from typing import Any, Dict, Tuple
from getpass import getpass

from python_custom_modules.helpers import create_file_from_template, execute_command
from python_custom_modules.terraform import Terraform


def run_terraform_to_configure_cloud(project_home: os, public_ssh_key: str) -> Tuple[Any, Any]:
    """Run terraform to get ssh_user and VMs ip addresses"""
    terraform = Terraform(project_home)
    print(f"\n{terraform.create_variable_tf()}")
    print(f"\n{terraform.create_ssh_user(public_ssh_key)}")
    print(f"SSH-user: {terraform.ssh_user}")
    terraform.run_script()
    print(f"\nVMs ip address: {terraform.vms_ip}")
    return terraform.ssh_user, terraform.vms_ip


def vm_status(ip_addr, attempts=5):
    """Get info if VM is up."""
    while attempts != 0:
        try:
            attempts -= 1
            conn = socket.create_connection((ip_addr, 22))
            print(conn)
            conn.close()  # close connection if server is up
            break
        except TimeoutError:
            if not attempts:
                raise Exception("VMs still not configured.")
        except ConnectionRefusedError:
            sleep(3)
            if not attempts:
                raise Exception("VMs still not configured.")


def create_and_copy_ssh_keys(path: os, user: str, vms_ip: Dict):
    """Create new ssh keys to communicate in LAN"""
    ip_address = dict(vms_ip)
    for ip in ip_address.values():
        vm_status(ip)

    jenkins_ip = ip_address.pop("jenkins")
    # Commands via ssh to create new ssh keys
    execute_command(f'ssh -o StrictHostKeyChecking=no -i "{path}/ssh_keys/service.key" {user}@{jenkins_ip} '
                    f'"ssh-keygen -t rsa -b 2048 -f ~/.ssh/id_rsa -q -N \'\'"')

    # Command via ssh to copy new public key to our project
    execute_command(f"scp -o StrictHostKeyChecking=no -i '{path}/ssh_keys/service.key'"
                    f" -r {user}@{jenkins_ip}:/home/{user}/.ssh/id_rsa.pub {path}/id_rsa.pub")

    for ip in ip_address.values():
        # Upload public key to postgres and liferay
        execute_command(f"scp -o StrictHostKeyChecking=no -i '{path}/ssh_keys/service.key'"
                        f" {path}/id_rsa.pub {user}@{ip}:/tmp/id_rsa.pub")
        execute_command(f"ssh -o StrictHostKeyChecking=no -i '{path}/ssh_keys/service.key' {user}@{ip}"
                        f" \"cat /tmp/id_rsa.pub >> ~/.ssh/authorized_keys\"")


def create_ssh_keys(home: os) -> str:
    """Create ssh key for project."""
    public_ssh_key = ""
    if not os.path.isdir(f"{home}/ssh_keys"):
        commands = [
            "mkdir ssh_keys",
            f"ssh-keygen -t rsa -b 2048 -f {home}/ssh_keys/service.key -q -N ''",
            f"cat {home}/ssh_keys/service.key.pub"
        ]
        for command in commands:
            public_ssh_key = execute_command(command)
        return public_ssh_key


def run_ansible(home: os, ip, ssh_user):
    ansible_home = f"{home}/ansible_jenkins_for_python"
    ansible_templates = f"{home}/python_templates/ansible_templates"

    host_variables = {
        "jenkins_ip": ip,
        "ssh_user": ssh_user,
        "ssh_private_key": f"{home}/ssh_keys/service.key"
    }
    print(host_variables)

    if not os.path.isdir(f"{ansible_home}/host_vars"):
        execute_command(f"mkdir {ansible_home}/host_vars")
        create_file_from_template(ansible_templates, "template_host.yaml",
                                  f"{ansible_home}/host_vars/jenkins-vm.yaml", host_variables)
    os.chdir(ansible_home)

    if not os.path.isfile("vault_pass.txt"):
        password = getpass("Input vault pass: ")
        with open("vault_pass.txt", 'w') as file:
            file.writelines(password)

    execute_command(f"scp -o StrictHostKeyChecking=no -i '{home}/ssh_keys/service.key'"
                    f" {ansible_home}/vault_pass.txt {ssh_user}@{ip}:/home/{ssh_user}/vault_pass.txt")

    print(execute_command(f"ansible-playbook playbooks/jenkins_pb.yaml -i hosts -v"
                          f" --vault-password-file {ansible_home}/vault_pass.txt"))


def main():
    project_home = os.path.dirname(os.path.abspath(__file__))  # get absolute path to start_up file

    os.chdir(project_home)

    public_ssh_key = create_ssh_keys(project_home)

    ssh_user, public_addresses = run_terraform_to_configure_cloud(project_home, public_ssh_key)

    create_and_copy_ssh_keys(project_home, ssh_user, public_addresses)

    run_ansible(project_home, public_addresses["jenkins"], ssh_user)
    print(public_addresses)


if __name__ == '__main__':
    main()
